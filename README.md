# README #

A very simple Windows command line tool that extracts tables from HTML files and saves them to a tab delimited text file.

## Usage: ##

```
#!Console

tabex.exe input.html output.txt
```