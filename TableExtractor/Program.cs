﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace TableExtractor
{
    class Program
    {
        private const char LineFeed = '\n';
        private const char CarriageReturn = '\r';
        private const char Tab = '\t';
        private const char Space = ' ';
        private const string EndRowTag = "</TR>";
        private const string RowPattern = @"<TR[^>]*>(.*|(.|\n)*)</TR>";
        private const string CellPattern = @"<TD[^>]*>([^<]*)</TD>";

        static StreamWriter outputWriter;

        static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                ExplainUsage();
                return;
            }

            string inputFileName = args[0];
            string outputFileName = args[1];

            Console.WriteLine("Extracting table rows and cells from HTML file...");
            Console.WriteLine("Input File: " + inputFileName);
            Console.WriteLine("Output File: " + outputFileName);

            ExtractTableFrom(inputFileName, outputFileName);

            Console.WriteLine("Done");
        }

        private static void ExplainUsage()
        {
            Console.WriteLine("Table Extractor (tabex)");
            Console.WriteLine("Usage:");
            Console.WriteLine("clean.exe input-file.html output-file.txt");
            Console.WriteLine("Outputs a tab delimited text file.");
        }

        private static void ExtractTableFrom(string htmlFileName, string outputFileName)
        {
            outputWriter = File.CreateText(outputFileName);

            string html = File.ReadAllText(htmlFileName);

            // Split at row terminator tags to prevent out of memory errors.
            var lines = html.Split(new string[] { EndRowTag }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string line in lines)
                ExtractRowFrom(line + EndRowTag);
            // Put the EndRowTag back so the RegEx pattern detects it.

            outputWriter.Flush();
            outputWriter.Close();
        }

        private static void ExtractRowFrom(string line)
        {
            var regex = new Regex(RowPattern, RegexOptions.Multiline | RegexOptions.IgnoreCase);
            var matches = regex.Matches(line);

            for (int r = 0; r < matches.Count; r++)
            {
                for (int i = 0; i < matches[r].Captures.Count; i++)
                {
                    string capturedValue = matches[r].Captures[i].Value;

                    string outputLine = ExtractCellsFrom(capturedValue);

                    outputWriter.WriteLine(outputLine);
                }
            }
        }

        private static string ExtractCellsFrom(string line)
        {
            string outputLine = string.Empty;

            var regex = new Regex(CellPattern, RegexOptions.Multiline | RegexOptions.IgnoreCase);
            var matches = regex.Matches(line);

            for (int i = 0; i < matches.Count; i++)
            {
                for (int ii = 0; ii < matches[i].Captures.Count; ii++)
                {
                    string cellValue = matches[i].Groups[1].Value;
                    cellValue = ReplaceSpecialChars(cellValue);
                    outputLine += cellValue.TrimEnd() + Tab;
                }
            }

            return outputLine;
        }

        private static string ReplaceSpecialChars(string text)
        {
            text = text.Replace(LineFeed, Space);
            text = text.Replace(CarriageReturn, Space);
            text = text.Replace(Tab, Space);
            return text;
        }
    }
}
